package com.agiletestingalliance;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class MinMaxTest 
{
    @Test
    public void testMinMax()
    {
	MinMax minMax = new MinMax();
        assertTrue( minMax.maximumNumber(10,20) == 20 );
        assertTrue( minMax.maximumNumber(20,10) == 20 );
    }

    @Test
    public void testUsefulness()
    {
	Usefulness usefulness = new Usefulness();
        assertTrue( usefulness.desc() != null );
    }

    @Test
    public void testDuration()
    {
	Duration duration = new Duration();
        assertTrue( duration.dur() != null );
    }

    @Test
    public void testAboutCPDOF()
    {
	AboutCPDOF aboutCPDOF = new AboutCPDOF();
        assertTrue( aboutCPDOF.desc() != null );
    }
}
